#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int map[10][10];
int level;
int ring_move;
int ring_ava;
int turns;


//create player i location and j location so that when the player move the character   the program wont have to find the player location using the loop
//2 more variable = more ram usage   but decrease the time using a loop
int player_i;
int player_j;

void print_map();
void reset_map();
int random();
void try_rand();
int play();
int dr_movable(int ,int ,int );
void move_dr(int ,int ,int );
void dark_rider_move();
int move_player(int );

int search_around(int ,int );

int check_wil_die();


//-----------------------------------------------------------------------------------------------------------------------


int main()
{
	srand(time(NULL));//if the user want a different ramdomly selected place of enimies
	int i,j;
	level = 1;
	int die = 0;
	
	do
	{
		reset_map();
		ring_move = 0;
		ring_ava = 1;
		turns = 1;
		
		if(play() != 1)
		{
			break;
		}
		
		level++;
		
	}while(true);
	
	
	system("cls");
	print_map();
	cout << endl;
	cout << "**********************************";
	cout << endl;
	cout << "You died at level " << level << endl;
	cout << "**********************************";
	
	do//to prevent user from accidental exit
	{
		cout << endl<< "type 99 to exit!";
		cin >> i;
	}while(i!=99);

}

//-----------------------------------------------------------------------------------------------------------------------

int play()
{
	int move;
	
	do
	{
		system("cls");
		print_map();
		cout << endl;
		cin >> move;
		//I will not print out an error if a player input a wrong number but it won't do anything instead
		if(move != 99 && move != 314159265)
		{
			switch(move_player(move))
			{
				case 1:
					//win
					return 1;
					break;
				case 0:
					//die
					return 0;
					break;
			}
		}
		else
		{
			if(ring_ava == 1 && move == 99)
			{
				ring_ava = 0;
				ring_move = 3;
			}
			else if(move == 314159265)//my own cheat  LOL
			{
				ring_ava = 0;
				ring_move = 9999999; 
			}
		}
		
		
		
	}while(true);
}


int move_player(int move)
{
	int i=player_i,j=player_j;
	
	if((move>=1&&move<=9)&&move!=5)//allowed move 1-9 exept 5
	{
		switch(move)
		{
			case 1:
				i+=1;
				j-=1;
				//  (/ down)
				break;
			case 2:
				i+=1;
				//  (down)
				break;
			case 3:
				i+=1;
				j+=1;
				//  (\ down)
				break;
			case 4:
				j-=1;
				//  (left)
				break;
			case 6:
				j+=1;
				//  (right)
				break;
			case 7:
				i-=1;
				j-=1;
				//  (\ up)
				break;
			case 8:
				i-=1;
				//  (up)
				break;
			case 9:
				i-=1;
				j+=1;
				//  (/ up)
				break;
			
			//there's no default because I already check with if()
			//and I did that to prevent the wrong input to not move the player but act like a turn so the dark rider moves  and that's cheating
		}
		
		
		if((i>=0&&i<=9) && (j>=0&&j<=9))//check still in table
		{
			
			if(map[i][j] == -11)//found exit
			{
				//win
				return 1;
			}
			else if(map[i][j] == 0)
			{
				//nothing  just move
				
				turns++;
				
				map[player_i][player_j] = 0;
				map[i][j] = -1;
				player_i = i;
				player_j = j;
				
				
				
				if(ring_move > 0)
				{
					ring_move--;
				}
				else
				{
					if(check_wil_die() == 1)
					{
						return 0;
					}
					dark_rider_move();
				}
			}
			else
			{
				//case the location is the dark rider location
				turns++;
				
				if(ring_move > 0)
				{
					ring_move--;
				}
				else
				{
					if(check_wil_die() == 1)
					{
						return 0;
					}
					dark_rider_move();
				}
			}
		}
		
		
		
	}
	return 111111111;//case no move
}






void dark_rider_move()
{
	int i,j;
	int move;

	
	for(i=0;i<10;i++)
	{
		for(j=0;j<10;j++)
		{
			if(map[i][j] >=1 && map[i][j] < turns)
			{
				do
				{
					move = random();
					
				}while((dr_movable(move,i,j)) == 0);
				move_dr(move,i,j);
				
			}
		}
	}
	
	
	
}

void move_dr(int move,int loc_i,int loc_j)
{
	int i = loc_i , j = loc_j;
	switch(move)
	{
		case 1:
			i+=1;
			j-=1;
			//  (/ down)
			break;
		case 2:
			i+=1;
			//  (down)
			break;
		case 3:
			i+=1;
			j+=1;
			//  (\ down)
			break;
		case 4:
			j-=1;
			//  (left)
			break;
		case 6:
			j+=1;
			//  (right)
			break;
		case 7:
			i-=1;
			j-=1;
			//  (\ up)
			break;
		case 8:
			i-=1;
			//  (up)
			break;
		case 9:
			i-=1;
			j+=1;
			//  (/ up)
			break;		
	}
	
	//cout << endl << "dr at " << loc_i << " " << loc_j << " Moved to " << i << " " << j << " move == " << move;
	map[i][j] = map[loc_i][loc_j]+1;
	map[loc_i][loc_j] = 0;
	
}

int dr_movable(int move,int i,int j)
{
	
	if((move>=1&&move<=9)&&move!=5)//allowed move 1-9 exept 5
	{
		switch(move)
		{
			case 1:
				i+=1;
				j-=1;
				//  (/ down)
				break;
			case 2:
				i+=1;
				//  (down)
				break;
			case 3:
				i+=1;
				j+=1;
				//  (\ down)
				break;
			case 4:
				j-=1;
				//  (left)
				break;
			case 6:
				j+=1;
				//  (right)
				break;
			case 7:
				i-=1;
				j-=1;
				//  (\ up)
				break;
			case 8:
				i-=1;
				//  (up)
				break;
			case 9:
				i-=1;
				j+=1;
				//  (/ up)
				break;
			
		}
		
		
		if((i>=0&&i<=9) && (j>=0&&j<=9) && map[i][j] == 0)//check still in table
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	else
	{
		return 0;
	}
	
	
	return 0;//just in case there's a bug 
}






int search_around(int i,int j)
{
	// a dr is at map[i][j]
	
	/*
	temp_i = i;
	temp_j = j;
	*/
	int temp_i;
	int temp_j;
	
	
	temp_i = i;
	temp_j = j;
	temp_i+=1;
	temp_j-=1;
	//  (/ down)
	if((temp_i>=0&&temp_i<=9) && (temp_j>=0&&temp_j<=9))
	{
		if(map[temp_i][temp_j] == -1)
		{
			map[temp_i][temp_j] = map[i][j];
			map[i][j] = 0;
			return 1;
		}
	}
	
	
	temp_i = i;
	temp_j = j;
	temp_i+=1;
	//  (down)
	if((temp_i>=0&&temp_i<=9) && (temp_j>=0&&temp_j<=9))
	{
		if(map[temp_i][temp_j] == -1)
		{
			map[temp_i][temp_j] = map[i][j];
			map[i][j] = 0;
			return 1;
		}
	}

	
	temp_i = i;
	temp_j = j;
	temp_i+=1;
	temp_j+=1;
	//  (\ down)
	if((temp_i>=0&&temp_i<=9) && (temp_j>=0&&temp_j<=9))
	{
		if(map[temp_i][temp_j] == -1)
		{
			map[temp_i][temp_j] = map[i][j];
			map[i][j] = 0;
			return 1;
		}
	}
	
	
	temp_i = i;
	temp_j = j;
	temp_j-=1;
	//  (left)
	if((temp_i>=0&&temp_i<=9) && (temp_j>=0&&temp_j<=9))
	{
		if(map[temp_i][temp_j] == -1)
		{
			map[temp_i][temp_j] = map[i][j];
			map[i][j] = 0;
			return 1;
		}
	}
	
	
	temp_i = i;
	temp_j = j;
	temp_j+=1;
	//  (right)
	if((temp_i>=0&&temp_i<=9) && (temp_j>=0&&temp_j<=9))
	{
		if(map[temp_i][temp_j] == -1)
		{
			map[temp_i][temp_j] = map[i][j];
			map[i][j] = 0;
			return 1;
		}
	}
	
	
	
	temp_i = i;
	temp_j = j;
	temp_i-=1;
	temp_j-=1;
	//  (\ up)
	if((temp_i>=0&&temp_i<=9) && (temp_j>=0&&temp_j<=9))
	{
		if(map[temp_i][temp_j] == -1)
		{
			map[temp_i][temp_j] = map[i][j];
			map[i][j] = 0;
			return 1;
		}
	}

	

	temp_i = i;
	temp_j = j;
	temp_i-=1;
	//  (up)
	if((temp_i>=0&&temp_i<=9) && (temp_j>=0&&temp_j<=9))
	{
		if(map[temp_i][temp_j] == -1)
		{
			map[temp_i][temp_j] = map[i][j];
			map[i][j] = 0;
			return 1;
		}
	}

	
	
	temp_i = i;
	temp_j = j;
	temp_i-=1;
	temp_j+=1;
	//  (/ up)
	if((temp_i>=0&&temp_i<=9) && (temp_j>=0&&temp_j<=9))
	{
		if(map[temp_i][temp_j] == -1)
		{
			map[temp_i][temp_j] = map[i][j];
			map[i][j] = 0;
			
			return 1;
		}
	}
	
	
	return 0;
	

}

int check_wil_die()
{
	int i,j;
	
	for(i=0;i<10;i++)
	{
		for(j=0;j<10;j++)
		{
			if(map[i][j] >= 1 && map[i][j] <=30)
			{
				if(search_around(i,j) == 1)
				{
					//die
					return 1;
				}
			}
		}
	}
	
	return 0;
}

void reset_map()
{
	
	int temp_i;
	int temp_j;
	
	/*
	temp_i = random();
	temp_j = random();
	*/
	
	//frodo == -1
	//exit == -11
	//dark rider 1-30
	
	
	for(int i=0;i<10;i++)
	{
		for(int j=0;j<10;j++)
		{
			map[i][j] = 0;
		}
	}
	
	//if I use the if(i==0&j==0) in the loop it would be a waste of time! sooo...
	map[0][0] = -1;//frodo
	map[9][9] = -11;//exit
	player_i = 0;
	player_j = 0;
	
	
	for(int i=1;i<=level*3;i++)//create dark rider
	{
		do
		{
			temp_i = random();
			temp_j = random();
		}while(map[temp_i][temp_j] != 0);
		map[temp_i][temp_j] = 1;
	}
	
}

void print_map()
{
	int i,j;
	
	
	
	for(i=0;i<10;i++)
	{
	
		cout << "---------------------";//for separating rows
		cout << endl;
		for(j=0;j<10;j++)
		{
			cout << "|";//separating the space
			if(map[i][j] == -1)
			{
				cout << "F";
				//cout << "#";
			}
			else if(map[i][j] == -11)
			{
				cout << "E";
				//cout << "*";
			}
			else if(map[i][j] == 0)
			{
				cout << " ";
			}
			else
			{
				cout << "R";
				//cout << map[i][j];
			}
			
			if(j==9)//add a separate line if it's the last column
			{
				cout << "|";
			}
		}
		cout << endl;
		
		if(i==9)
		{
			cout << "---------------------";//add a separate line if it's the last row
		}
		
	}
	
	//added ring bar
	cout << endl;
	if(ring_move > 0)
	{
		cout << "Special ability move left : " << ring_move << endl;
	}
	
	if(ring_ava == 1)
	{
		cout << "Special ability charge : Available      (Type 99 to use the ability!)" << endl;
	}
	
}














int random()
{
	return (rand() % 9) + 1;
}


void try_rand()
{
	int x;
	do
	{
		cin >> x;
		cout << endl << "Number " << random() << endl;
	}while(x != -1);
}